package com.example.dell.repaso;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editText1, editText2, editText3, editText4, editText5;
    Button buscar, guardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //tomar el control de los elementos graficos
        editText1=(EditText)findViewById(R.id.cedula);
        editText2=(EditText)findViewById(R.id.nombre);
        editText3=(EditText)findViewById(R.id.apellido);
        editText4=(EditText)findViewById(R.id.fechadenacimiento);
        editText5=(EditText)findViewById(R.id.materia);
        buscar=(Button)findViewById(R.id.Buscar);
        guardar=(Button) findViewById(R.id.Guardar);



    }

    public void guardar(View view){
        omar o=new omar(this, "datos", null,1 );
        SQLiteDatabase  bdd =o.getWritableDatabase();
        ContentValues registro = new ContentValues();

        registro.put("cedula",editText1.getText().toString());
        registro.put("nombre",editText2.getText().toString());
        registro.put("apellido",editText3.getText().toString());
        registro.put("fechadenacimiento",editText4.getText().toString());
        registro.put("materia",editText5.getText().toString());
        bdd.insert("tabla",null,registro);
        bdd.close();
        editText2.setText("");
        editText3.setText("");
        editText4.setText("");
        editText5.setText("");

    }
    public void buscar(View view){
        omar p=new omar(this,"datos", null, 1);
        SQLiteDatabase bdd=p.getWritableDatabase();
        editText1=(EditText)findViewById(R.id.cedula);
        String cedu=  editText1.getText().toString();
        Log.e("as",cedu);

        String query ="select * from tabla where cedula='"+cedu+"'";
        Log.e("aaaaaa", query);
        if(!cedu.isEmpty()){
            Cursor FILA = bdd.rawQuery(query, null);
            Log.e("aaaaaaaa",String.valueOf(FILA));
            if (FILA.moveToFirst()){
                editText2=(EditText)findViewById(R.id.nombre);
                editText3=(EditText)findViewById(R.id.apellido);
                editText4=(EditText)findViewById(R.id.fechadenacimiento);
                editText5=(EditText)findViewById(R.id.materia);

                editText2.setText(FILA.getString(1));
                editText3.setText(FILA.getString(2));
                editText4.setText(FILA.getString(3));
                editText5.setText(FILA.getString(4));

            }
            else {
                bdd.close();
            }
        }


    }

    public void modificar(){}


}
